<?php

ob_start();

@session_start();

include('config/Database.php');
$database = new Database();
$db = $database->getConnection(); 

?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Products List | Machine Test</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Easy Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="js/wow.min.js"></script>
	<script>
		 new WOW().init();
	</script>
<!--//end-animate-->
<!----webfonts--->
<link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
<!---//webfonts---> 
 <!-- Meters graphs -->
<script src="js/jquery-1.10.2.min.js"></script>
<!-- Placed js at the end of the document so the pages load faster -->

</head> 
   
 <body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
    <!-- left side start-->
		<div class="left-side sticky-left-side">

			<!--logo and iconic logo start-->
			<div class="logo">
				<h1><a href="index.html">Easy <span>Admin</span></a></h1>
			</div>
			<div class="logo-icon text-center">
				<a href="index.html"><i class="lnr lnr-home"></i> </a>
			</div>

			<!--logo and iconic logo end-->
			<div class="left-side-inner">

				<?php

				  include('side_menu.php');

				  ?>
			</div>
		</div>
    <!-- left side end-->
    
    <!-- main content start-->
		<div class="main-content main-content4">
			<!-- header-starts -->
			<div class="header-section">
			 
			<!--toggle button start-->
			<a class="toggle-btn  menu-collapsed"><i class="fa fa-bars"></i></a>
			<!--toggle button end-->

			<!--notification menu start -->
			<div class="menu-right">
				<div class="user-panel-top">  	
					
					<div class="profile_details">		
						<ul>
							<li class="dropdown profile_details_drop">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<div class="profile_img">	
										<span style="background:url(images/1.jpg) no-repeat center"> </span> 
										 <div class="user-name">
											<p><?php echo $_SESSION['store_name']; ?><span>Administrator</span></p>
										 </div>
										 <i class="lnr lnr-chevron-down"></i>
										 <i class="lnr lnr-chevron-up"></i>
										<div class="clearfix"></div>	
									</div>	
								</a>
								<ul class="dropdown-menu drp-mnu">									
									<li> <a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a> </li>
								</ul>
							</li>
							<div class="clearfix"> </div>
						</ul>
					</div>		
					           	
					<div class="clearfix"></div>
				</div>
			  </div>
			<!--notification menu end -->
			</div>
	<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
					<h3 class="blank1">Product details</h3>
					<?php 

			         if(isset($_GET['successMessage']) && !empty($_GET['successMessage'])){ ?>
			         <div class="generic-title text-center wow bounceInUp" data-wow-duration="2s">

			            <h3 style="color:green;"><?php echo $_GET['successMessage']; ?></h2> 

			         </div>

			         <?php } ?>
					 <div class="xs tabls">
						<button class="btn-success btn"><a href="register-product.php" style="color:white;">+ Add new product</a></button>
					   <div class="panel-body1">
					   <table class="table">
						 <thead>
							<tr>
							  <th>#</th>
							  <th>First Name</th>
							  <th>Description</th>
							  <th>Actions</th>
							</tr>
						  </thead>
						  <tbody>
						  	<?php
							$count=0;	
							$cust_sql="SELECT products.* FROM products WHERE  product_status='1' order by product_id DESC";
							

							$cust_res	= mysqli_query($db,$cust_sql) or die(mysqli_error($db));
							if ($cust_res->num_rows > 0) {
							while($cust_row=mysqli_fetch_array($cust_res)){

								$count++;

								$product_id = $cust_row['product_id'];						 

							?>
							<tr>
							  <th scope="row"><?php echo $count; ?></th>
							  <td><?php echo $cust_row['product_name']; ?></td>
							  <td><?php echo $cust_row['product_description'];?></td>
							  <td><a style="display: flex;flex-direction: row;width:auto; float:left;margin-right:10px;color:blue;" href="register-product.php?id=<?php echo $cust_row['product_id']; ?>">Edit</a><a style="display: flex;flex-direction: row;width:auto; float:left;margin-right:10px;color:blue;" href="delete-product.php?id=<?php echo $cust_row['product_id']; ?>">Delete</a></td>
							</tr>
							<?php
							}
							} else { ?>
        						<td>No data found</td>
    						<?php 
    						}
							?> 
						  </tbody>
						</table>
						</div>												
					</div>
				</div>
			</div>
		</div>
		<!--footer section start-->
			<footer>
			   <p></p>
			</footer>
        <!--footer section end-->
	</section>
	
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.min.js"></script>
</body>
</html>