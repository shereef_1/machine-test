<?php
session_start();
set_time_limit(0);
include('config/Database.php');
$database = new Database();
$db = $database->getConnection(); 
$error	= 0;

if(isset($_POST["subbtn"]) && $_POST["subbtn"]=="Login") {
      //$txt_username= $_POST['txt_username'];
      $username=$_REQUEST['txt_username'];
		$email=$_REQUEST['txt_username'];
      $txt_userpass= $_POST['txt_userpass'];
		$hashed_password = password_hash($txt_userpass, PASSWORD_BCRYPT);

      $error	= 0;
      $sql		= "SELECT * from users where user_password = '$hashed_password' and user_email='$email' or user_name='$username' and user_status='1'";
      //echo $sql; die;
      $res=mysqli_query($db,$sql) or die($sql.mysqli_error($db));
      if(mysqli_num_rows($res)== 0){
        $error=1;
        $errmsg="Invalid User Name or Password !";
	  } else {
        $redirect = NULL;
        $myrow=mysqli_fetch_array($res);
		
        $vendor_id			 = $myrow["user_id"];
        $user_email			 = $myrow["user_email"]; 
		  $user_first_name	 = $myrow["user_name"];
		        
    	  $_SESSION['user_id']		= $vendor_id;
        $_SESSION['store_name']		= $user_first_name;
		    $_SESSION['vendor_email']	= $user_email;			 
        
       
        //$cur_time=$LOCALTime;
		    session_write_close();
		    
        $msg = "<meta http-equiv=\"Refresh\" content=\"0;url=dashboard.php\">";
        echo $msg;        
        exit;
    }
  }
?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Sign | Machine Test</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Easy Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="js/wow.min.js"></script>
	<script>
		 new WOW().init();
	</script>
<!--//end-animate-->
<!----webfonts--->
<link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
<!---//webfonts---> 
 <!-- Meters graphs -->
<script src="js/jquery-1.10.2.min.js"></script>
<!-- Placed js at the end of the document so the pages load faster -->
<style>
    .form-signin-heading{
		margin-bottom:12px;}
    
	.username{
		margin-bottom:15px;}
    </style>
<script language="javascript">

function check_login()
  {
  frm=document.form1;
  if(frm.txt_username.value=="")
    {
    alert("Please Enter Username");
    frm.txt_username.focus();
    return false;
    }
  else if(frm.txt_userpass.value=="")
    {
    alert("Please Enter Password");
    frm.txt_userpass.focus();
    return false;
    }
      return true;
    }
     

</script>
</head> 
   
 <body class="sign-in-up">
    <section>
			<div id="page-wrapper" class="sign-in-wrapper">
				<div class="graphs">
					<div class="sign-in-form">
						<div class="sign-in-form-top">
							<p><span>Sign In to</span> <a href="#">Admin</a></p>
						</div>
						<div class="signin">
							<form name="form1" method="post" action="sign-in.php" onSubmit="javascript: return check_login()">
							<h2 class="form-signin-heading">
					        <?php
					          if($error==1)
					          {
					      ?>
					        <span><font color="#ff0000" face="arial" size="2"><b><?php echo $errmsg;?></b></font></span>
					      <?php
					          }
					      ?>
					      </h2>
							<div class="log-input">
								<div class="log-input-left">
								   <input type="text" class="user" placeholder="Username/Email address" name="txt_username"/>
								</div>
								<div class="clearfix"> </div>
							</div>
							<div class="log-input">
								<div class="log-input-left">
								   <input type="password" class="lock" placeholder="Password" name="txt_userpass"/>
								</div>
								<div class="clearfix"> </div>
							</div>
							<input type="submit" name="subbtn" value="Login">
						</form>	 
						</div>
						<div class="new_people">
							<a href="sign-up.php">Register Now!</a>
						</div>
					</div>
				</div>
			</div>
		<!--footer section start-->
			<footer>
			   <p></p>
			</footer>
        <!--footer section end-->
	</section>
	
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.min.js"></script>
</body>
</html>