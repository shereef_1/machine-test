<?php

ob_start();

@session_start();



	

include('config/Database.php');
$database = new Database();
$db = $database->getConnection(); 

$user_id	= $_SESSION["user_id"];



if (isset($_GET['id']) && !empty($_GET['id'])) {
   $getSingleProductDataSql = "SELECT * from products WHERE product_id =".$_GET['id'];
   $result = mysqli_query($db,$getSingleProductDataSql) or die(mysqli_error($db));
   $prdtData = mysqli_fetch_array($result);  

}

?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Register product | Machine Test</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Easy Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="js/wow.min.js"></script>
	<script>
		 new WOW().init();
	</script>
<!--//end-animate-->
<!----webfonts--->
<link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
<!---//webfonts---> 
 <!-- Meters graphs -->
<script src="js/jquery-1.10.2.min.js"></script>
<!-- Placed js at the end of the document so the pages load faster -->

</head> 
   
 <body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
    <!-- left side start-->
		<div class="left-side sticky-left-side">

			<!--logo and iconic logo start-->
			<div class="logo">
				<h1><a href="index.html">Easy <span>Admin</span></a></h1>
			</div>
			<div class="logo-icon text-center">
				<a href="index.html"><i class="lnr lnr-home"></i> </a>
			</div>

			<!--logo and iconic logo end-->
			<div class="left-side-inner">

				 <?php

				  include('side_menu.php');

				  ?>
			</div>
		</div>
    <!-- left side end-->
    
    <!-- main content start-->
		<div class="main-content main-content3">
			<!-- header-starts -->
			<div class="header-section">
			 
			<!--toggle button start-->
			<a class="toggle-btn  menu-collapsed"><i class="fa fa-bars"></i></a>
			<!--toggle button end-->

			<!--notification menu start -->
			<div class="menu-right">
				<div class="user-panel-top">  	
					
					<div class="profile_details">		
						<ul>
							<li class="dropdown profile_details_drop">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<div class="profile_img">	
										<span style="background:url(images/1.jpg) no-repeat center"> </span> 
										 <div class="user-name">
											<p><?php echo $_SESSION['store_name']; ?><span>Administrator</span></p>
										 </div>
										 <i class="lnr lnr-chevron-down"></i>
										 <i class="lnr lnr-chevron-up"></i>
										<div class="clearfix"></div>	
									</div>	
								</a>
								<ul class="dropdown-menu drp-mnu">									
									<li> <a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a> </li>
								</ul>
							</li>
							<div class="clearfix"> </div>
						</ul>
					</div>		
					           	
					<div class="clearfix"></div>
				</div>
			  </div>
			<!--notification menu end -->
			</div>
	<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
					<h3 class="blank1">Register products</h3>
					<?php 
			         if(isset($_GET['errorMessage']) && !empty($_GET['errorMessage'])){ ?>
			         <div class="generic-title text-center wow bounceInUp" data-wow-duration="2s">

			            <h3 style="color:red;"><?php echo $_GET['errorMessage']; ?></h2> 

			         </div>
			         <?php } ?>
			         <?php 

			         if(isset($_GET['successMessage']) && !empty($_GET['successMessage'])){ ?>
			         <div class="generic-title text-center wow bounceInUp" data-wow-duration="2s">

			            <h3 style="color:green;"><?php echo $_GET['successMessage']; ?></h2> 

			         </div>

			         <?php } ?>
						<div class="tab-content">
						<div class="tab-pane active" id="horizontal-form">
							<form class="form-horizontal" action="register-product-form.php" method="post" enctype="multipart/form-data">
								<input type="hidden" name="product_id" value="<?php if (!empty($prdtData['product_id'])) {echo $prdtData ['product_id'];} ?>">
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Product name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control1" id="product_name" name="product_name" placeholder="Product name" value="<?php if (!empty($prdtData['product_name'])) {echo $prdtData ['product_name'];} ?>">
									</div>
								</div>
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Description</label>
									<div class="col-sm-8">
										<textarea class="form-control1" id="product_description" name="product_description" placeholder="Description"><?php if (!empty($prdtData['product_description'])) {echo $prdtData ['product_description'];} ?></textarea>
									</div>
								</div>
								<div class="panel-footer">
								<div class="row">
									<div class="col-sm-8 col-sm-offset-2">
										<button class="btn-success btn">Submit</button>
										<button class="btn-default btn">Cancel</button>
									</div>
								</div>
							 </div>
							</form>
						</div>
					</div>
					
	
						
				</div>
			</div>
		</div>
		<!--footer section start-->
			<footer>
			   <p></p>
			</footer>
        <!--footer section end-->
	</section>
	
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.min.js"></script>
</body>
</html>