<?php

class Database{
	
	private $host  = 'localhost';
    private $user  = 'root';
    private $password   = "";
    private $database  = "machine_test"; 

    public function getConnection(){        
        $conn = new mysqli($this->host, $this->user, $this->password, $this->database);
        mysqli_set_charset($conn,"utf8");
        if($conn->connect_error){
            die("Error failed to connect to MySQL: " . $conn->connect_error);
        } else {
            return $conn;
        }
    }
}
?>