<?php


include('config/Database.php');

$database = new Database();
$db = $database->getConnection(); 


$name = $_POST["user_name"];
$email = $_POST["user_email"];
$password = $_POST["user_password"];
// $hashed_password = password_hash($password, PASSWORD_DEFAULT);
$hashed_password = password_hash($password, PASSWORD_BCRYPT);

$redirectTOPage = "Machine-test/sign-up.php";

if(!isset($name) || empty($name) || is_null($name) || $name == "0"){
    header('Location:/'.$redirectTOPage.'?errorMessage=Please enter username');
    exit;
}
if(!isset($email) || empty($email) || is_null($email) || $email == "0"){
    header('Location:/'.$redirectTOPage.'?errorMessage=Please enter email');
    exit;
}
if(!isset($password) || empty($password) || is_null($password) || $password == "0"){
    header('Location:/'.$redirectTOPage.'?errorMessage=Please enter password');
    exit;
}

$ins_ad_sql     = "INSERT INTO users (user_name,user_email,user_password,user_status) 
            VALUES ('$name','$email','$hashed_password','1')";

$ins_ad_res     = mysqli_query($db,$ins_ad_sql) or print(mysqli_error($db));

if($ins_ad_res != "1"){

header('Location:/'.$redirectTOPage.'?errorMessage=Could not save - '.mysqli_error($db));

}
else{

    header('Location:/'.$redirectTOPage.'?successMessage=New registration has been successfully done');
}
exit;

?>